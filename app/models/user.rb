# frozen_string_literal: true

# Model User
class User < ApplicationRecord
  require 'bcrypt'

  before_create :set_encrypt_password

  validates :username, presence: true, uniqueness: true, length: { in: 3..20 }

  has_many :courses

  def set_encrypt_password
    self.password = BCrypt::Password.create password
  end

  def valid_password?(password)
    self.password = BCrypt::Engine.hash_secret(password, self.password)
  end
end
