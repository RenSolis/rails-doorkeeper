# frozen_string_literal: true

Doorkeeper.configure do
  orm :active_record

  resource_owner_authenticator do
    raise "Please configure doorkeeper resource_owner_authenticator block located in #{__FILE__}"
  end

  admin_authenticator do
  end

  resource_owner_from_credentials do |routes|
    user = User.find_by(username: params[:username])
    if user && user&.valid_password?(params[:password])
      user
    end
  end

  use_refresh_token
end

Doorkeeper.configuration.token_grant_types << 'password'
