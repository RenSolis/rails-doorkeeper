# frozen_string_literal: true

Rails.application.routes.draw do
  root 'users#new'
  use_doorkeeper
  resources :courses
  resources :users

  #============= API =============
  namespace :api, defaults: { format: :json } do
    resources :courses, only: :index
  end
end
